import { Button, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function Banner(props) {

  const {title, subtitle} = props
  const navigate = useNavigate();

  const handleButtonClick = () => {
    navigate("/courses"); // Call navigate with the desired URL to navigate
  };
 
  return (
    <Row id="row1">
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{subtitle}</p>
        <Button variant="primary" onClick={handleButtonClick}>Enroll now!</Button>
      </Col>
    </Row>
  );
} 

 