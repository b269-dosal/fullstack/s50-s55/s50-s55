import {useEffect, useState} from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


// [S51 Activity Start]
export default function CourseCard({ course }) {
  const { name, description, price, _id } = course;
  //const [count, setCount] = useState(0);
  //const [slots, setSlots] = useState(30);

/*   function enroll() {
   //  if (slots > 0) { // Check if there are available slots
     // setCount(count + 1);
      //setSlots(slots - 1);
    //} else {
     // alert("No More Seats"); // Show alert if there are no more slots
    //} 
     // setCount(count + 1);
     // setSlots(slots - 1);

  } */

/*   useEffect(() => {
    if(slots <= 0) {
        alert("No more seats available!")
    }
  }, [slots]) */

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title><h4>{name}</h4>  </Card.Title>
            <Card.Subtitle><strong>Description</strong></Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle><strong>Price</strong></Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            {/* //<Card.Text><strong>Enrollees</strong></Card.Text>
            //<Card.Subtitle>{count} Enrollees</Card.Subtitle>
            //<Card.Text>{slots} Available</Card.Text>
            //<Button variant="success" onClick={enroll} disabled={slots<=0}>Enroll</Button> */}
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );

}
