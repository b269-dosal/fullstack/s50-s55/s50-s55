import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { useLocation } from 'react-router-dom';


 
 export default function Home() {
    const location = useLocation();
    const isHomePage = location.pathname === '/';
  
    switch (isHomePage) {
      case true:
        return (
          <>
            <div className="App">
              <Banner
                title="Zuitt Coding Bootcamp"
                subtitle="Opportunities for everyone, everywhere."
                buttonText="Enroll now!"
              />
            </div>
            <Highlights />
          </>
        );
    }
  }


