import Swal from 'sweetalert2';
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';

// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Login(){

    // "useContext" hook that allows you to access and update the shared state from any component in your application.
    // "UserContext" - global state that can be shared and updated throughout the application
    const {user, setUser} = useContext(UserContext);

    // to store values of the input fields
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // hook returns a function that lets you navigate to components
    // const navigate = useNavigate()

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if(typeof data.access !== "undefined") {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
                            
              /*   Swal.fire({
                    title: "Login Succesful!",
                    icon: "Success",
                    text: "Welcome to Zuitt!"
                }) */

                Swal.fire({
                    title: "Welcome to Zuitt!",
                    icon: "success",
                    text: "Login Successful!",
                    showCloseButton: true, // Show close button
                    confirmButtonText: "OK", // Change confirm button text
                    footer: "Thank you for logging in!", // Add a footer text
                    timer: 5000, // Auto close after 5 seconds
                    timerProgressBar: true, // Show progress bar for timer
                    allowOutsideClick: false, // Prevent clicking outside the modal
                    showLoaderOnConfirm: true, // Show loading spinner on confirm
                    preConfirm: () => { // Add custom function on confirm
                        return new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                            }, 2000); // Wait for 2 seconds before resolving
                        });
                    }
                }).then(() => {
                    // Custom function to execute after alert is closed
                    console.log("SweetAlert closed!");
                }).catch(Swal.noop); // Prevent console error when clicking outside modal
                
         }  
          else {

            Swal.fire({
                title: "Authenticated Failed!",
                icon: "error",
                text: "Please check your login details and try again"
            })
        }

        //////////  My Swal.fire

       /*  Swal.fire({
            title: "Authentication Failed!",
            text: "Please check your login details and try again",
            icon: "error",
            confirmButtonText: "OK",
            confirmButtonColor: "#FF0000",
            customClass: {
                title: 'my-custom-title-class',
                content: 'my-custom-content-class',
                confirmButton: 'my-custom-confirm-button-class'
            },
            showCloseButton: true,
            closeButtonHtml: '<i class="fas fa-times"></i>',
            footer: '<a href="#">Forgot password?</a>',
            backdrop: `
                rgba(0,0,0,0.8)
                center left
                no-repeat
            `,
            timer: 5000,
            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            cancelButtonColor: "#808080",
            reverseButtons: true,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve();
                    }, 2000);
                });
            }
        }).then((result) => {
            if (result.isConfirmed) {
                // Code to handle confirmed button click
            } else if (result.isDenied) {
                // Code to handle denied button click
            } else if (result.dismiss === Swal.DismissReason.timer) {
                // Code to handle timer dismiss
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Code to handle cancel dismiss
            } else if (result.dismiss === Swal.DismissReason.backdrop) {
                // Code to handle backdrop dismiss
            }
        }).catch((error) => {
            // Code to handle error
        }); */
        
        //////////////

    });

        // Set the email of the authenticated user in the local storage
        // Syntax
        // localStorage.setItem('propertyName', value);
        // localStorage.setItem('email', email);
        // Sets the global user state to have properties obtain from local storage
        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('')
        setPassword('')
        // navigate('/')

        // alert("Successfully login!")

        
};
    const retrieveUserDetails = (token) => {
            // The token will be sent as part of the request's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Global user state for validation accross the whole app
                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        };

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
        <Navigate to="/courses" />
        :
        <Form onSubmit={e => authenticate(e)} id="form">
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
};







//[SECTION - ACTIVITY 52]
/*
export default function Login() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect (() => {
        if (email !== "" && password !== "") {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    function loginUser(e) {
        e.preventDefault();

        // Clear input fields
        setEmail("");
        setPassword("");

        alert("Successful login!");
    }

    return (
        <Form onSubmit={(e) => loginUser(e)}>
            <Form.Group controlId="userEmail">
            <h2>LOGIN</h2>
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
       
            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn"  style={{ marginTop: "10px" }}>
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" style={{ marginTop: "10px" }} disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}
 */