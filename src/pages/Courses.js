import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard"
//import coursesData from "../data/coursesData"

export default function Courses() {
    
    const [courses, setCourses] = useState([]);	
	
    /*    console.log(coursesData)
    const courses = coursesData.map(course => {
        return  (
                    <CourseCard key={course.id} course = {course}/>
                )
        
    })
 */
    useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])


    return (
        <>
        {/* We to declare props drilling */}
        {/* We can pass information from one component to another using props (props drilling)  */}
        {/*  Curly braces {} are used for props to signify that we are provisind passing information from one component to another using JS expression */} 
        {courses}
        </>
    )
}

