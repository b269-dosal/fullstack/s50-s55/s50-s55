import {useEffect, useState} from 'react'
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import {Container} from 'react-bootstrap';
import Home from './pages/Home';
import Courses from './pages/Courses'; 
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './components/Errorpage';

import CourseView from './components/CourseView';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';



export default function App() {


  // state hook for the user state that defined here is for global scope
  // to store the user information and will used for validating if user is logged in on the app or not
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  //const [user, setUser] = useState({email: localStorage.getItem('email')});
  

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to chck if user information is porper;y stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

      // User is Logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);


  return (
    // <></> fragments - common pattern in React for component to return multiple element
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
        
          <Container>
            
            <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/> 
            <Route path="/register" element={<Register/>}/> 
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/*" element={<Error/>}/>
            <Route path="/courses/:courseId" element={<CourseView/>}/>

            </Routes>  
            
          </Container> 

        </Router>
      </UserProvider>
    </>
  );
}

